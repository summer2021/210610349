import numpy as np
import mindquantum as mq
from mindquantum.gate import H, X, RZ, RX, SWAP
from mindquantum import Circuit, Hamiltonian
import mindspore as ms
import mindspore.context as context
from mindspore.common.parameter import Parameter
from mindspore.common.initializer import initializer
from mindquantum.nn import generate_pqc_operator
from mindquantum.ops import QubitOperator
from mindquantum.nn.mindquantum_ansatz_only_layer import MindQuantumAnsatzOnlyLayer

num = 14
dim = 4
testt = 12345

def sym(enc, n):
    for i in range(num - n):
        enc += SWAP.on([i, i + n])
    return enc

def ham(n):
    res = QubitOperator('')
    for i in range(num):
        j = (i + 1) % num
        res += QubitOperator('X%d X%d' % (i, j), 0.25)
        res += QubitOperator('Y%d Y%d' % (i, j), 0.25)
        res += QubitOperator('Z%d Z%d' % (i, j), 0.25)
    return res

encoder = Circuit()
#encoder part
for i in range(num):
    encoder += X.on(i)
for i in range(0, num, 2):
    encoder += H.on(i)
    encoder += X.on(i + 1, i)
#ansatz part
ansatz = Circuit()
for k in range(0, dim):
    for i in range(0, num):
        j = (i + 1) % num
        ansatz += X.on(i, j)
        ansatz += RX('p%d' % (k * num + i)).on(j)
        ansatz += X.on(i)
        ansatz += RZ({'p%d' % (k * num + i): -0.5}).on(i)
        ansatz += X.on(i)
        ansatz += X.on(i, j)

heisham = ham(num)

total_circuit = encoder + ansatz
total_circuit.summary()
ansatz_parameter_names = ansatz.para_name
print(ansatz_parameter_names)

heispqcnet = MindQuantumAnsatzOnlyLayer(ansatz_parameter_names, total_circuit, Hamiltonian(heisham))
initial_energy = heispqcnet()
print("Initial energy: %20.16f" % (initial_energy.asnumpy()))

optimizer = ms.nn.Adagrad(heispqcnet.trainable_params(), learning_rate=4e-2)
train_pqcnet = ms.nn.TrainOneStepCell(heispqcnet, optimizer)

eps = 1.e-5
print("eps: ", eps)
energy_diff = eps * 1000
energy_last = initial_energy.asnumpy() + energy_diff
iter_idx = 0
while (abs(energy_diff) > eps):
    energy_i = train_pqcnet().asnumpy()
    if iter_idx % 5 == 0:
        print("Step %3d energy %20.16f" % (iter_idx, float(energy_i)))
    energy_diff = energy_last - energy_i
    energy_last = energy_i
    iter_idx += 1

print("Optimization completed at step %3d" % (iter_idx - 1))
print("Optimized energy: %20.16f" % (energy_i))
print("Optimized amplitudes: \n", heispqcnet.weight.asnumpy())